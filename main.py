import sys
import time

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from window.logindialog import LoginDialog


def main(argv):
    app = QApplication(argv,True)

    dialog = LoginDialog()
    dialog.show()

    app.connect(app, SIGNAL("lastWindowClosed()"), app, SLOT("quit()"))

    sys.exit(app.exec_())
 
if __name__ == "__main__":
    main(sys.argv)