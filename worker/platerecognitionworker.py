from PyQt4.QtCore import *
from PyQt4.QtGui import *
from numpy import *

import cv2
from utils.videostream import VideoStream
from utils.config_loader import load_config
from plate_recognition.plate import PlateRecognizer

class PlateRecognitionWorker(QThread):

    plate_result = pyqtSignal(str)
    
    def __init__(self, parent=None):
        super(PlateRecognitionWorker, self).__init__()
        
        self.config = load_config()

        self.vs = VideoStream(self.config['VIDEO_SOURCE']['plate_src'])
        self.vs.start()

        self.engine = PlateRecognizer()
        


    def run(self):
        while True:
            frame = self.vs.read()
            if frame is not None:
                result = self.engine.recognize(frame)
                
                if result is not None:
                    self.plate_result.emit(result)
                    


                
                
                

                