from PyQt4.QtCore import *
from PyQt4.QtGui import *
from numpy import *

from speeddetector.detector import Speed
import cv2
from utils.videostream import VideoStream
from utils.config_loader import load_config

class SpeedDetectorWorker(QThread):

    overspeed = pyqtSignal(int)
    speed_detected = pyqtSignal(int)


    def __init__(self, parent=None):
        super(SpeedDetectorWorker, self).__init__()
        
        self.config = load_config()

        self.vs = VideoStream(self.config['VIDEO_SOURCE']['speed_src'])
        self.vs.start()

        self.speed_detector_engine = Speed()
        

        self.current_crop_image = None
        self.current_thresh_image = None

    def run(self):
        while True:
            frame = self.vs.read()
            if frame is not None:
                crop_image = self.preprocess_image(frame)

                if crop_image is not None:
                    gray = cv2.cvtColor(crop_image, cv2.COLOR_BGR2GRAY)
                    
                    ave_speed, thresh_image = self.speed_detector_engine.detect_speed(gray)

                    if ave_speed is not None:
                        self.speed_detected.emit(ave_speed)
                        # if ave_speed > int(self.config["SPEED"]["threshold"]):
                        #     self.overspeed.emit(ave_speed)
                        # else:
                        #     self.speed_detected.emit(ave_speed)

                    if thresh_image is not None:
                        # image = cv2.cvtColor(thresh_image, cv2.COLOR_GRAY2BGR)
                        # image = cv2.resize(image, (533,143))
                        self.current_thresh_image = thresh_image

                    self.current_crop_image = crop_image
                    

    
    def preprocess_image(self, frame):
            if "CROP_AREA" in self.config:
                ix =  int(self.config["CROP_AREA"]["first_point_x"])
                iy = int(self.config["CROP_AREA"]["first_point_y"])
                x = int(self.config["CROP_AREA"]["second_point_x"])
                y = int(self.config["CROP_AREA"]["second_point_y"])
                
                image = frame[iy:y, ix:x]
                image = cv2.resize(image, (320,240))


                return image
            else:
                return None
                
                
                

                