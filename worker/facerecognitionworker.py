from face_recognition.recognition import FaceRecognition

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from numpy import *
import time
import uuid
import cv2
from os import path, makedirs

from utils.videostream import VideoStream
from utils.config_loader import load_config

class FaceRecognitionWorker(QThread):

    recognition_finish = pyqtSignal(str, ndarray)
    detected_face = pyqtSignal(str, str)

    def __init__(self, parent=None):
        super(FaceRecognitionWorker, self).__init__()

        self.config = load_config()

        self.vs = VideoStream(self.config['VIDEO_SOURCE']['gate_src'])
        self.vs.start()

        self.face_recognition_engine = FaceRecognition()
        self.start_time = None
        self.current_id = str(uuid.uuid4())

    def run(self):
        while True:
            frame = self.vs.read()
            if frame is not None: 
                frame = frame[234:411+234, 658:1264]
                largest_face = self.face_recognition_engine.find_largest_face(frame)
                
                if largest_face is not None:
                    result = self.face_recognition_engine.recognize(frame)
                    print(result)
                    if result is not None:
                        identity, face_points = result

                        if identity == "?":
                            self.detected_face.emit(path.join("face_detected", self.current_id), "VISITOR")
                        else:    
                            self.detected_face.emit(path.join("face_detected", self.current_id), "NOT_VISITOR")
                    
                    self.start_time = time.time()
                    current_path = path.join("face_detected", self.current_id)

                    if not path.exists(current_path):
                        makedirs(current_path)
                    image_name = str(uuid.uuid4())

                    ix,iy,x,y = largest_face
                    image = frame[iy:y, ix:x]

                    cv2.imwrite(path.join(current_path, image_name + ".png"), image)

                else:
                    if self.start_time is not None:
                        if time.time() - self.start_time > 3.0:
                            
                            self.start_time = time.time()
                            self.current_id = str(uuid.uuid4())

                    
                # result = self.face_recognition_engine.recognize(frame)

                # if result is not None:
                #     identity, largest_face_points = result
                #     ix,iy,x,y = largest_face_points
                #     image = frame[iy:y, ix:x]
                #     self.recognition_finish.emit(identity, image)