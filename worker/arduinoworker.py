from PyQt4.QtCore import *
import serial
from serial.tools import list_ports
import time

MANUFACTURER = "Arduino LLC (www.arduino.cc)"
BAUDRATE = 9600

class SmsWorker(QThread):

    def __init__(self, parent=None):
        super(SmsWorker, self).__init__()
        self.__serial_port = None
        self.__find_arduino()

    def run(self):
        while True:
            print(self.__serial_port.readline())
            time.sleep(.100)

    def __find_arduino(self):
        for port in list_ports.comports():        
            if MANUFACTURER == port.manufacturer:
                self.__serial_port =  serial.Serial(port.device, BAUDRATE)
    

    def send_sms(self, reciepient, message):
        _write_bytes = ':' + reciepient + ':' + message
        print(_write_bytes)
        self.__serial_port.write(bytes(_write_bytes, 'utf-8'))