from speeddetector.detector import Speed
import cv2
from utils.videostream import VideoStream
from utils.config_loader import load_config

config = load_config()

vs = VideoStream(config['VIDEO_SOURCE']['speed_src'])
vs.start()
font = cv2.FONT_HERSHEY_SIMPLEX
speed_detector_engine = Speed()
current_speed = 0

def preprocess_image(frame):
    if "CROP_AREA" in config:
        ix =  int(config["CROP_AREA"]["first_point_x"])
        iy = int(config["CROP_AREA"]["first_point_y"])
        x = int(config["CROP_AREA"]["second_point_x"])
        y = int(config["CROP_AREA"]["second_point_y"])

        image = frame[iy:y, ix:x]
        image = cv2.resize(image, (320,240))

        return image
    else:
        return None

while True:
    frame = vs.read()
    if frame is not None:
        
        crop_image = preprocess_image(frame)

        cv2.imshow("PROCESSING FRAME", crop_image)

        if crop_image is not None:
            gray = cv2.cvtColor(crop_image, cv2.COLOR_BGR2GRAY)

            ave_speed, thresh_image = speed_detector_engine.detect_speed(gray)

            if ave_speed is not None:
                current_speed = ave_speed
            cv2.putText(frame,"Speed :" + str(int(current_speed)) + "kph",(30,30), font, 0.8,(255,0,0),2,cv2.LINE_AA)
        
            if thresh_image is not None:
                cv2.imshow("MOTION FRAME", thresh_image)
            
            cv2.imshow("MAIN FRAME", frame) 
    
    cv2.waitKey(1)
            