# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_number_dialog(object):
    def setupUi(self, number_dialog):
        number_dialog.setObjectName(_fromUtf8("number_dialog"))
        number_dialog.setWindowModality(QtCore.Qt.ApplicationModal)
        number_dialog.resize(400, 316)
        number_dialog.setStyleSheet(_fromUtf8(""))
        self.verticalLayoutWidget = QtGui.QWidget(number_dialog)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(0, 0, 401, 301))
        self.verticalLayoutWidget.setObjectName(_fromUtf8("verticalLayoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label = QtGui.QLabel(self.verticalLayoutWidget)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout.addWidget(self.label)
        self.phonenumber_txt = QtGui.QLineEdit(self.verticalLayoutWidget)
        self.phonenumber_txt.setStyleSheet(_fromUtf8(""))
        self.phonenumber_txt.setAlignment(QtCore.Qt.AlignCenter)
        self.phonenumber_txt.setCursorMoveStyle(QtCore.Qt.LogicalMoveStyle)
        self.phonenumber_txt.setObjectName(_fromUtf8("phonenumber_txt"))
        self.verticalLayout.addWidget(self.phonenumber_txt)
        self.proceed_btn = QtGui.QPushButton(self.verticalLayoutWidget)
        self.proceed_btn.setStyleSheet(_fromUtf8(""))
        self.proceed_btn.setObjectName(_fromUtf8("proceed_btn"))
        self.verticalLayout.addWidget(self.proceed_btn)

        self.retranslateUi(number_dialog)
        QtCore.QMetaObject.connectSlotsByName(number_dialog)

    def retranslateUi(self, number_dialog):
        number_dialog.setWindowTitle(_translate("number_dialog", "Login", None))
        self.label.setText(_translate("number_dialog", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; color:#000000;\">Welcome to</span></p><p align=\"center\"><span style=\" font-size:16pt; font-weight:600; color:#000000;\">Speed Detector System</span></p></body></html>", None))
        self.phonenumber_txt.setPlaceholderText(_translate("number_dialog", "Enter phone number", None))
        self.proceed_btn.setText(_translate("number_dialog", "Proceed", None))

