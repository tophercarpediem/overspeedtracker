# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'calibrate.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_calibrate_dialog(object):
    def setupUi(self, calibrate_dialog):
        calibrate_dialog.setObjectName(_fromUtf8("calibrate_dialog"))
        calibrate_dialog.resize(697, 622)
        self.gridLayoutWidget = QtGui.QWidget(calibrate_dialog)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 681, 611))
        self.gridLayoutWidget.setObjectName(_fromUtf8("gridLayoutWidget"))
        self.gridLayout = QtGui.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.label_4 = QtGui.QLabel(self.gridLayoutWidget)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.horizontalLayout_5.addWidget(self.label_4)
        self.second_point_x = QtGui.QSpinBox(self.gridLayoutWidget)
        self.second_point_x.setObjectName(_fromUtf8("second_point_x"))
        self.horizontalLayout_5.addWidget(self.second_point_x)
        self.horizontalLayout_4.addLayout(self.horizontalLayout_5)
        self.horizontalLayout_6 = QtGui.QHBoxLayout()
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        self.label_5 = QtGui.QLabel(self.gridLayoutWidget)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.horizontalLayout_6.addWidget(self.label_5)
        self.second_point_y = QtGui.QSpinBox(self.gridLayoutWidget)
        self.second_point_y.setObjectName(_fromUtf8("second_point_y"))
        self.horizontalLayout_6.addWidget(self.second_point_y)
        self.horizontalLayout_4.addLayout(self.horizontalLayout_6)
        self.gridLayout.addLayout(self.horizontalLayout_4, 1, 0, 1, 1)
        self.label = QtGui.QLabel(self.gridLayoutWidget)
        self.label.setEnabled(True)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 12, 0, 1, 1)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label_2 = QtGui.QLabel(self.gridLayoutWidget)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout.addWidget(self.label_2)
        self.first_point_x = QtGui.QSpinBox(self.gridLayoutWidget)
        self.first_point_x.setObjectName(_fromUtf8("first_point_x"))
        self.horizontalLayout.addWidget(self.first_point_x)
        self.horizontalLayout_3.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_3 = QtGui.QLabel(self.gridLayoutWidget)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_2.addWidget(self.label_3)
        self.first_point_y = QtGui.QSpinBox(self.gridLayoutWidget)
        self.first_point_y.setObjectName(_fromUtf8("first_point_y"))
        self.horizontalLayout_2.addWidget(self.first_point_y)
        self.horizontalLayout_3.addLayout(self.horizontalLayout_2)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)
        self.pushButton = QtGui.QPushButton(self.gridLayoutWidget)
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.gridLayout.addWidget(self.pushButton, 3, 0, 1, 1)
        self.pushButton_2 = QtGui.QPushButton(self.gridLayoutWidget)
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.gridLayout.addWidget(self.pushButton_2, 4, 0, 1, 1)

        self.retranslateUi(calibrate_dialog)
        QtCore.QMetaObject.connectSlotsByName(calibrate_dialog)

    def retranslateUi(self, calibrate_dialog):
        calibrate_dialog.setWindowTitle(_translate("calibrate_dialog", "Calibrate", None))
        self.label_4.setText(_translate("calibrate_dialog", "second_point_x:", None))
        self.label_5.setText(_translate("calibrate_dialog", "second_point_y:", None))
        self.label.setText(_translate("calibrate_dialog", "Image", None))
        self.label_2.setText(_translate("calibrate_dialog", "first_point_x:", None))
        self.label_3.setText(_translate("calibrate_dialog", "first_point_y:", None))
        self.pushButton.setText(_translate("calibrate_dialog", "Apply settings", None))
        self.pushButton_2.setText(_translate("calibrate_dialog", "Load previous settings", None))

