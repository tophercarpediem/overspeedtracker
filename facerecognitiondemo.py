from face_recognition.recognition import FaceRecognition
from utils.videostream import VideoStream
import cv2

engine = FaceRecognition()
# vs = VideoStream("demo\zeke.mp4")
# vs.start()

cap = cv2.VideoCapture(0)

while True:

    ret, stream = cap.read()

    if stream is not None:

        res = engine.recognize(stream)

        if res is not None:
            identity, face_points = res
            x,y,w,h = face_points

            cv2.rectangle(stream, (x,y), (w,h), (0,255,0), 2)
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(stream,identity.upper(),(x,y), font, 2,(255,0,0),2,cv2.LINE_AA)

        cv2.imshow('FaceRecognition Engine', stream)
        cv2.waitKey(1)

        print(res)
