import time
import cv2

from utils.config_loader import load_config

class Speed(object):
    def __init__(self):
        print("Loading SpeedDetector engine")
        self.ave_speed = 0.0
        self.frame_count = 0
        self.fps_time = time.time()
        self.first_event = True
        self.event_timer = time.time()
        self.start_pos_x = None
        self.end_pos_x = None
        self.prev_pos_x = None
        self.travel_direction = ""
        self.font = cv2.FONT_HERSHEY_SIMPLEX

        self.track_count = 0
        self.speed_list = []

        self.last_frame = None
        
        length_in_mm = float(load_config()['SPEED']['length_in_mm'])

        px_to_kph = float(length_in_mm/90 * 0.0036)
        self.speed_conv = 0.621371 * px_to_kph

    def get_contour(self, image, grayimage1):

        if grayimage1 is not None:

            # Get differences between the two greyed images
            differenceimage = cv2.absdiff(grayimage1, image)

            # Blur difference image to enhance motion vectors
            differenceimage = cv2.blur(differenceimage, (10, 10))

            # Get threshold of blurred difference image
            # based on THRESHOLD_SENSITIVITY variable
            retval, thresholdimage = cv2.threshold(differenceimage,
                                                20,
                                                255, cv2.THRESH_BINARY)

            thresholdimage, contours, hierarchy = cv2.findContours(thresholdimage,
                                                                    cv2.RETR_EXTERNAL,
                                                                    cv2.CHAIN_APPROX_SIMPLE)

            # Update grayimage1 to grayimage2 ready for next image2
            
            return image, contours, thresholdimage
        
        return None, None



    def detect_speed(self, frame):

        if self.last_frame is None:

            self.last_frame = frame
        else:

            grayimage1, contours, thresholdimage = self.get_contour(frame, self.last_frame)
            self.last_frame = grayimage1
            
            if contours:
                #print('Countour found')
                total_contours = len(contours)
                motion_found = False
                biggest_area = 100

                for c in contours:
                    # get area of contour
                    found_area = cv2.contourArea(c)
                    if found_area > biggest_area:
                        (x, y, w, h) = cv2.boundingRect(c)
                        cur_track_time = time.time() # record cur track time
                        track_x = x
                        track_y = y
                        track_w = w  # movement width of object contour
                        track_h = h  # movement height of object contour
                        motion_found = True
                        biggest_area = found_area

                if motion_found:

                    #print('Motion is found')
                    
                    # Check if last motion event timed out
                    reset_time_diff = time.time() - self.event_timer
                    if  reset_time_diff > 0.3:
                        # event_timer exceeded so reset for new track
                        self.event_timer = time.time()
                        self.first_event = True
                        self.start_pos_x = None
                        self.prev_pos_x = None
                        self.end_pos_x = None
                        #print("Reset - event_timer", reset_time_diff, ">", "0.3 sec Exceeded ")


                    ##############################
                    # Process motion events and track object movement
                    ##############################
                    if self.first_event:   # This is a first valid motion event
                        self.first_event = False  # Only one first track event
                        self.track_start_time = cur_track_time # Record track start time
                        self.prev_start_time = cur_track_time
                        self.start_pos_x = track_x
                        self.prev_pos_x = track_x
                        self.end_pos_x = track_x
                        
                        self.event_timer = time.time() # Reset event timeout
                        self.track_count = 0
                        self.speed_list = []

                    else:
                        self.prev_pos_x = self.end_pos_x
                        self.end_pos_x = track_x

                        if self.end_pos_x - self.prev_pos_x > 0:
                            self.travel_direction = "L2R"
                            #print(self.travel_direction)
                        else:
                            self.travel_direction = "R2L"
                            #print(self.travel_direction)

                        if (abs(self.end_pos_x - self.prev_pos_x) > 1 and
                            abs(self.end_pos_x - self.prev_pos_x) < 20):

                            try:
                                self.track_count += 1
                                
                                cur_track_dist = abs(self.end_pos_x - self.prev_pos_x)
                                cur_ave_speed = float((abs(cur_track_dist /
                                                    float(abs(cur_track_time -
                                                                self.prev_start_time)))) *
                                                                self.speed_conv)

                                self.speed_list.append(cur_ave_speed)
                                self.prev_start_time = cur_track_time
                                self.event_timer = time.time()
                                #print('Track count:', self.track_count)


                                if self.track_count >= 5:
                                    tot_track_dist = abs(track_x - self.start_pos_x)
                                    tot_track_time = abs(self.track_start_time - cur_track_time)
                                    
                                    ave_speed = sum(self.speed_list) / float(len(self.speed_list))
                                    #print('Average speed:', ave_speed)
                                    self.track_count = 0
                                    self.start_pos_x = None
                                    self.end_pos_x = None
                                    self.first_event = True # Reset Track
                                    self.event_timer = time.time()
                                    horz_line = '============================================================='
                                    #print(horz_line)
                                    return ave_speed, thresholdimage
                            except ZeroDivisionError as ex:
                                pass
            return None, thresholdimage
        return None, None


        


