from keras.engine import Model
from keras import models
from keras import layers
from keras.layers import Input
from keras.preprocessing import image

from keras_vggface.vggface import VGGFace
from keras_vggface import utils

from imutils import face_utils

import scipy as sp
import cv2
import os
import glob
import pickle
import cvlib as cv
import numpy as np
import tensorflow as tf


class FaceRecognition(object):
    def __init__(self):
        print("Loading FaceRecognition engine")
        print("Loading the model")
        self.model = VGGFace(model='resnet50', include_top=False, input_shape=(224, 224, 3), pooling='avg')
        
        self.graph = tf.get_default_graph()
        print("Loading precompute feature maps")
        self.precompute_features_map = self.load_feature_maps(
            './data/precompute_features.pickle')

    def load_feature_maps(self, filename):
        feature_maps_file = open(filename, "rb")
        feature_maps = pickle.load(feature_maps_file)
        feature_maps_file.close()
        return feature_maps


    def find_largest_face(self, frame):
        
        
        largest_face = None
        current_largest_area = 0
        
        if frame is not None:
            faces, _ = cv.detect_face(frame)

            for face in faces:
                ix,iy,x,y = face
                
                width = x - ix
                height = y - iy

                area = width * height

                if area > current_largest_area:
                    current_largest_area = area
                    largest_face = face

        return largest_face

    def recognize(self, frame):

        largest_face_points = self.find_largest_face(frame)
        
        identity = None

        if largest_face_points is not None:
            ix,iy,x,y = largest_face_points

            # CROP FACE
            crop_face = frame[iy:y, ix:x]
            
            if crop_face is not None:
                try:
                    resize_image = cv2.resize(crop_face, (224,224))
                    image_tensor = np.expand_dims(resize_image, axis=0)
                    with self.graph.as_default():
                        features_faces = self.model.predict(image_tensor)
                        identity = self.identify_face(features_faces)

                except Exception:
                    return None
        else:
            return None

        return identity, largest_face_points


    def identify_face(self, features, threshold=100):
        distances = []
        for person in self.precompute_features_map:
            person_features = person.get("features")
            distance = sp.spatial.distance.euclidean(person_features, features)
            distances.append(distance)

        min_distance_value = min(distances)
        min_distance_index = distances.index(min_distance_value)
        
        # print("=======================================================")
        # print("Predicted class ", self.precompute_features_map[min_distance_index].get("name"))
        # print("Minimun distance ", min_distance_value)

        if min_distance_value < threshold:
            return self.precompute_features_map[min_distance_index].get("name")
        else:
            return "?"
