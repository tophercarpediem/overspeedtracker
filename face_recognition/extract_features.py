import imutils
from imutils.video import VideoStream
import cv2
import os
import glob
import pickle
from keras_vggface.vggface import VGGFace
import numpy as np
from keras.preprocessing import image
from keras_vggface import utils
import time
import argparse

vs = VideoStream(0)
vs.start()
time.sleep(2)

face_detector = cv2.CascadeClassifier(".\\pretrained_models\\haarcascade_frontalface_alt.xml")

def crop_face(imgarray, section, margin=20, size=224):

    img_h, img_w, _ = imgarray.shape
    if section is None:
        section = [0, 0, img_w, img_h]
    (x, y, w, h) = section
    margin = int(min(w,h) * margin / 100)
    x_a = x - margin
    y_a = y - margin
    x_b = x + w + margin
    y_b = y + h + margin
    if x_a < 0:
        x_b = min(x_b - x_a, img_w-1)
        x_a = 0
    if y_a < 0:
        y_b = min(y_b - y_a, img_h-1)
        y_a = 0
    if x_b > img_w:
        x_a = max(x_a - (x_b - img_w), 0)
        x_b = img_w
    if y_b > img_h:
        y_a = max(y_a - (y_b - img_h), 0)
        y_b = img_h
    cropped = imgarray[y_a: y_b, x_a: x_b]
    resized_img = cv2.resize(cropped, (size, size), interpolation=cv2.INTER_AREA)
    resized_img = np.array(resized_img)
    return resized_img, (x_a, y_a, x_b - x_a, y_b - y_a)

frame_counter = 0
while True:
    frame = vs.read()

    if frame is not None:
        
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = face_detector.detectMultiScale(
                gray,
                scaleFactor=1.2,
                minNeighbors=10,
                minSize=(64, 64))
        
        face = None

        if len(faces) > 1:
            face = max(faces, key=lambda rectangle: (rectangle[2] * rectangle[3]))  # area = w * h
            

        elif len(faces) == 1:
            face = faces[0]

        if face is not None:
            frame_counter = frame_counter + 1
            face_img, cropped = crop_face(frame, face, margin=40, size=224)
            (x, y, w, h) = cropped
            cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 200, 0), 2)
            cv2.imshow('Faces', frame)
            imgfile = os.path.basename("face").replace(".","_") +"_"+ str(frame_counter) + ".png"
            imgfile = os.path.join("faces", "jet", imgfile)
            cv2.imwrite(imgfile, face_img)

        if(frame_counter == 320):
            break

        if cv2.waitKey(5) == 27:
            break
