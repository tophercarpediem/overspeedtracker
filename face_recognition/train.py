from keras_vggface.vggface import VGGFace
import numpy as np
from keras.preprocessing import image
import glob
from keras_vggface import utils
import os
import cv2
import pickle

resnet50_features = VGGFace(model='resnet50', include_top=False, input_shape=(224, 224, 3),
                            pooling='avg')
def image2x(image_path):
    img = image.load_img(image_path, target_size=(224, 224))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = utils.preprocess_input(x, version=1)
    return x

def cal_mean_feature(image_folder):
    face_images = list(glob.iglob(os.path.join(image_folder, '*.*')))

    def chunks(l, n):
        for i in range(0, len(l), n):
            yield l[i:i + n]

    batch_size = 32
    face_images_chunks = chunks(face_images, batch_size)
    fvecs = None

    for face_images_chunk in face_images_chunks:
        print("Getting feature vectors: {im}/{le}".format(im=batch_size, le=len(face_images)))

        images = np.concatenate([image2x(face_image) for face_image in face_images_chunk])        
        batch_fvecs = resnet50_features.predict(images)
        if fvecs is None:
            fvecs = batch_fvecs
        else:
            fvecs = np.append(fvecs, batch_fvecs, axis=0)
        batch_size += 32
    
    
    return np.array(fvecs).sum(axis=0) / len(fvecs)

folders = list(glob.iglob(os.path.join("faces", '*')))
names = [os.path.basename(folder) for folder in folders]

precompute_features = []
for i, folder in enumerate(folders):
    name = names[i]
    save_folder = os.path.join("faces", name)
    mean_features = cal_mean_feature(image_folder=save_folder)
    print(mean_features.shape)
    precompute_features.append({"name": name, "features": mean_features})
    
save_stuff = open("./data/precompute_features.pickle", "wb")
pickle.dump(precompute_features, save_stuff)
save_stuff.close()