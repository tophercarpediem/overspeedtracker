from PyQt4.QtCore import *
from PyQt4.QtGui import *
import cv2

from layout.calibrate import Ui_calibrate_dialog
from utils.config_loader import *

class CalibrateDialog(QDialog, Ui_calibrate_dialog):

    def __init__(self):
        QDialog.__init__(self)
        self.setupUi(self)

        self.first_point_x.valueChanged.connect(self.__update_points)
        self.first_point_y.valueChanged.connect(self.__update_points)
        self.second_point_x.valueChanged.connect(self.__update_points)
        self.second_point_y.valueChanged.connect(self.__update_points)

        self.pushButton.clicked.connect(self.__apply_settings)
        self.pushButton_2.clicked.connect(self.__load_settings)

        self.first_point_x.setRange(0, 640)
        self.first_point_y.setRange(0, 480)
        self.second_point_x.setRange(0, 640)
        self.second_point_y.setRange(0, 480)

        self.calibrate_frame = None

    def __update_points(self, value):

        bottom_left = (int(self.first_point_x.text()), int(self.second_point_y.text()))
        upper_right = (int(self.second_point_x.text()), int(self.first_point_y.text()))
        upper_left = (int(self.first_point_x.text()), int(self.first_point_y.text()))
        bottom_right = (int(self.second_point_x.text()), int(self.second_point_y.text()))

        
        copy = self.calibrate_frame.copy()
        cv2.circle(copy,(upper_left), 10, (0,0,255), -1)
        cv2.circle(copy,(bottom_right), 10, (0,0,255), -1)
        #cv2.circle(copy,(bottom_left), 10, (0,0,255), -1)
        #cv2.circle(copy,(upper_right), 10, (0,0,255), -1)

        cv2.rectangle(copy,bottom_right,upper_left,(0,255,0),3)
        self.draw_frame(copy)

    def draw_frame(self, frame):
        if frame is not None:
            if self.calibrate_frame is None:
                self.calibrate_frame = frame

            image = frame
            #image = cv2.resize(frame, (532,289))
            image = QImage(image, image.shape[1],
                            image.shape[0], image.shape[1] * 3, QImage.Format_RGB888).rgbSwapped()

            pix = QPixmap(image)
            self.label.setPixmap(pix)

    def __apply_settings(self, val):

            config = load_config()
            config["CROP_AREA"] = {
                'first_point_x':self.first_point_x.text(),
                'first_point_y': self.first_point_y.text(),
                'second_point_x':self.second_point_x.text(),
                'second_point_y':self.second_point_y.text(),
            }
            update_config(config)
    
    def __load_settings(self, evt):
        config = load_config()
        if "CROP_AREA" in config:
            self.first_point_x.setValue(int(config["CROP_AREA"]["first_point_x"]))
            self.first_point_y.setValue(int(config["CROP_AREA"]["first_point_y"]))
            self.second_point_x.setValue(int(config["CROP_AREA"]["second_point_x"]))
            self.second_point_y.setValue(int(config["CROP_AREA"]["second_point_y"]))
        else:
            QMessageBox.critical(self,
                "Error", "No settings avalable. Please define one!",
                QMessageBox.Ok)