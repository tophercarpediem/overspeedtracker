import cv2

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from layout.main_layout import Ui_MainWindow
from worker.facerecognitionworker import FaceRecognitionWorker
from worker.speeddetectorworker import SpeedDetectorWorker
from plate_recognition.plate import PlateRecognizer
from worker.platerecognitionworker import PlateRecognitionWorker
from worker.arduinoworker import SmsWorker

from utils.videostream import VideoStream
import time

import os
import random
import datetime

from utils.config_loader import load_config


class MainWindow(QMainWindow, Ui_MainWindow):

        calibrate_key_pressed = pyqtSignal()

        def __init__(self):
            QMainWindow.__init__(self)

            self.setupUi(self)

            self._horizontal_line = '============================================='

            self.face_list = []
            self.speed_list = []
            self.is_already_show = False
            self.time_speed_detected = None
            self.speed_start_time = None

            self.queue = []

            self.current_face_id = ''

            wid = QWidget()

            self.form = QFormLayout()

            wid.setLayout(self.form)

            self.history_contianer.setWidget(wid)
            self.config = load_config()

        def showEvent(self, event):
            print('showEvent')
            super(MainWindow, self).showEvent(event)

            if not self.is_already_show:
                time.sleep(2)
                self.is_already_show = True
                self.initialize_video_streams()

        def keyPressEvent(self, event):
            key = event.key()

            if key == 81:  # 81 is letter q
                self.plate_recognition_worker.engine.release_memory()
                self.close()
            elif key == 67:  # 67 is letter c
                self.calibrate_key_pressed.emit()

        def initialize_video_streams(self):

            # Face Recognition
            self.face_recognition_worker = FaceRecognitionWorker()
            self.face_recognition_worker.recognition_finish.connect(
                self.recognition_finish)
            self.face_recognition_worker.detected_face.connect(
                self.__detect_face)
            self.face_recognition_worker.start()

            # Speed Detection
            self.speed_detector_worker = SpeedDetectorWorker()
            self.speed_detector_worker.overspeed.connect(
                self.__overspeeding_vehicle)
            self.speed_detector_worker.speed_detected.connect(
                self.__motion_detected)
            self.speed_detector_worker.start()

            # Plate Recognition
            # self.plate_recognition_stream = PlateRecognitionWorker('plate480p.mp4')
            # self.plate_recognition_stream.start()
            self.plate_recognition_worker = PlateRecognitionWorker()
            self.plate_recognition_worker.plate_result.connect(
                self.__plate_result)
            self.plate_recognition_worker.start()

            self.sms_worker = SmsWorker()
            self.sms_worker.start()

            self.thread = QThread()
            self.timer = QTimer()
            self.timer.timeout.connect(self.__update_streams)
            self.timer.start(30)

            self.timer.moveToThread(self.thread)
            self.thread.start()

        def recognition_finish(self, name, frame):

            self.visitor.setStyleSheet("")
            self.not_visitor.setStyleSheet("")

            image = cv2.resize(frame, (149, 127))
            image = QImage(image, image.shape[1],
                           image.shape[0], image.shape[1] * 3, QImage.Format_RGB888).rgbSwapped()

            pix = QPixmap(image)
            self.crop_face.setPixmap(pix)

            if name == "?":
                self.visitor.setStyleSheet("background-color:red;")
            else:
                self.not_visitor.setStyleSheet("background-color:red;")

        def __update_streams(self):

            image = self.face_recognition_worker.vs.read()
            speed_stream = self.speed_detector_worker.vs.read()
            thresh_stream = self.speed_detector_worker.current_thresh_image
            crop_stream = self.speed_detector_worker.current_crop_image
            plate_stream = self.plate_recognition_worker.vs.read()

            if image is not None:
                width = 532
                height = 289

                image = cv2.resize(image, (width, height))
                image = QImage(image, image.shape[1],
                               image.shape[0], image.shape[1] * 3, QImage.Format_RGB888).rgbSwapped()

                pix = QPixmap(image)
                self.gate.setPixmap(pix)

            if speed_stream is not None:
                width = 532
                height = 289

                image = cv2.resize(speed_stream, (width, height))
                image = QImage(image, image.shape[1],
                               image.shape[0], image.shape[1] * 3, QImage.Format_RGB888).rgbSwapped()

                pix = QPixmap(image)
                self.road.setPixmap(pix)

            if crop_stream is not None:
                width = 532
                height = 143

                image = cv2.resize(crop_stream, (width, height))
                image = QImage(image, image.shape[1],
                               image.shape[0], image.shape[1] * 3, QImage.Format_RGB888).rgbSwapped()

                pix = QPixmap(image)
                self.crop_area.setPixmap(pix)

            if thresh_stream is not None:
                width = 532
                height = 143

                image = cv2.cvtColor(thresh_stream, cv2.COLOR_GRAY2BGR)
                image = cv2.resize(image, (width, height))

                image = QImage(image, image.shape[1],
                               image.shape[0], image.shape[1] * 3, QImage.Format_RGB888).rgbSwapped()

                pix = QPixmap(image)
                self.threshold_image.setPixmap(pix)

            if plate_stream is not None:
                width = 532
                height = 289

                image = cv2.resize(plate_stream, (width, height))
                image = QImage(image, image.shape[1],
                               image.shape[0], image.shape[1] * 3, QImage.Format_RGB888).rgbSwapped()

                pix = QPixmap(image)
                self.plate.setPixmap(pix)

            #if self.speed_start_time is not None:
            if len(self.queue) > 0:
                self.current_face_id = self.queue[0]['face_id']
                if time.time() - self.queue[0]['start_time'] > 20.0:
                    self.queue.pop(0)

        def __motion_detected(self, speed):
            print("Speed:", speed)
            label = QLabel('Time: ' + str(datetime.datetime.now().time()
                                          ) + ' || Speed: ' + str(speed) + 'kph')
            self.form.insertRow(0, label)

            if self.time_speed_detected is None:
                self.time_speed_detected = time.time()

            if len(self.queue) > 0:
                print(self.queue)
                props = self.queue[0]
                if 'speed_list' not in props:
                    props['speed_list'] = []
                props['speed_list'].append(speed)

                self.queue[0] = props
            #self.speed.setText("Speed: " + str(speed) + "kph")

        def __overspeeding_vehicle(self, speed):
            print("OverSpeed:", speed)

            #print(self.plate_recognition_worker.recognize(self.plate_recognition_stream.read()))

        def __plate_result(self, plate_number):
            max_speed = 0
            print(self.queue)
            if len(self.queue) > 0:
                max_speed = max(self.queue[0]['speed_list'])

            if len(self.queue) > 0:

                image = self.__choose_random_image(self.queue[0]['face_id'])
                image = cv2.imread(os.path.join(
                    self.queue[0]['face_id'], image))

                identity = self.queue[0]['identity']
                #result = self.face_recognition_worker.face_recognition_engine.recognize(image)
                width = self.crop_face.frameGeometry().width()
                height = self.crop_face.frameGeometry().height()

                image = cv2.resize(image, (width, height))

                image = QImage(image, image.shape[1],
                               image.shape[0], image.shape[1] * 3, QImage.Format_RGB888).rgbSwapped()

                pix = QPixmap(image)

                self.crop_face.setPixmap(pix)
                self.plate_number.setText(str(plate_number).upper())
                self.speed.setText("Speed: " + str(max_speed) + "kph")
                print(identity)

                if identity == "NOT_VISITOR":
                    if max_speed > int(self.config['SPEED']['threshold']):
                        self.sms_worker.send_sms(self.config['GUARD_CONFIG']['phone_number'], str(
                            max_speed) + ' ' + plate_number + ' NOT_VISITOR\n')
                    self.not_visitor.setStyleSheet('background-color: red;')
                else:
                    if max_speed > int(self.config['SPEED']['threshold']):
                        self.sms_worker.send_sms(self.config['GUARD_CONFIG']['phone_number'], str(
                            max_speed) + ' ' + plate_number + ' VISITOR\n')
                    self.visitor.setStyleSheet('background-color: red;')

                if len(self.queue) > 0:
                    if self.current_face_id == self.queue[0]['face_id']:
                        self.queue.pop(0)

        def __detect_face(self, face_id, identity):
            if self.current_face_id != face_id:
                self.current_face_id = face_id
                print(face_id)
                queue = {}
                queue['face_id'] = face_id
                queue['start_time'] = time.time()
                queue['identity'] = identity
                self.speed_start_time = time.time()
                self.queue.append(queue)

            for q in self.queue:
                if q['face_id'] == face_id:
                    print("Altering identity of latest one: ", identity)
                    q['identity'] = identity

        def __choose_random_image(self, directory):
            return random.choice(os.listdir(directory))
