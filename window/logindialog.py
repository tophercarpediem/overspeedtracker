from PyQt4.QtCore import *
from PyQt4.QtGui import *
from layout.entry import Ui_number_dialog
from utils.config_loader import *

from .mainwindow import MainWindow
from .calibratedialog import CalibrateDialog

class LoginDialog(QDialog, Ui_number_dialog):

    def __init__(self):

        QDialog.__init__(self)
        self.setupUi(self)
        
        self.wnd = MainWindow()
        self.calibrate_dialog = CalibrateDialog()

        self.phonenumber_txt.clearFocus()
        self.phonenumber_txt.setAutoFillBackground(False)

        self.proceed_btn.clicked.connect(self.__handle_proceed)
        self.wnd.calibrate_key_pressed.connect(self.open_calibrate_dialog)
        self.calibrate_dialog.pushButton.clicked.connect(self.open_main_wnd)
    
    def __handle_proceed(self, evt):
        self.phonenumber_txt.text()

        if len(self.phonenumber_txt.text()) != 13:
            QMessageBox.critical(self,
                "Invalid phone number", "Please input 11-digit number",
                QMessageBox.Ok)
        else:
            config = load_config()

            config["GUARD_CONFIG"] = {
                'PHONE_NUMBER': self.phonenumber_txt.text()
            }
            update_config(config)
            
            #self.wnd.initialize_video_streams()
            self.wnd.showFullScreen()
            
            self.hide()
    
    def open_calibrate_dialog(self):
        self.calibrate_dialog.show()
        self.calibrate_dialog.draw_frame(self.wnd.speed_detector_worker.vs.read())
        self.wnd.hide()

    def open_main_wnd(self, evt):
        self.wnd.speed_detector_worker.config = load_config()
        self.wnd.show()
        self.calibrate_dialog.close()