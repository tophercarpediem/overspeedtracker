
import numpy as np
import cv2
from openalpr import Alpr


class PlateRecognizer(object):

    def __init__(self, locality="us", configuration="openalpr.conf", runtime="runtime_data"):
        self.load_plate_recognition(locality, configuration, runtime)

    def load_plate_recognition(self, locality, configuration, runtime):
        self.alpr = Alpr(locality, configuration, runtime)
        if not self.alpr.is_loaded():
            print('Error loading OpenALPR')
            sys.exit(1)

        self.alpr.set_top_n(3)

    def recognize(self, frame):

        success, image = cv2.imencode('.png', frame)

        results = self.alpr.recognize_array(image.tobytes())
        best_candidate = None

        for i, plate in enumerate(results['results']):
            best_candidate = plate['candidates'][0]['plate'].upper()

        return best_candidate

    def release_memory(self):
        self.alpr.unload()
